#include "math.h"
#include "cmath"
#include <eigen3/Eigen/Geometry>
#include <tf2/LinearMath/Transform.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <iostream>
#include <fstream>

#include "MathOp/Transform.hpp"

using namespace std;

namespace MathOp
{

Transform::Transform()
{
	matrix_.setIdentity();
}

Transform::Transform(const tf2::Transform& matrix)
{
    tf2::Vector3 trans = matrix.getOrigin();
    tf2::Matrix3x3 rot = matrix.getBasis();
    for (int r = 0; r < 3; r++)
    {
        for (int c = 0; c < 3; c++)
        {
            matrix_(r,c) = rot[r][c];
        }
        matrix_(r,3) = trans[r];
    }
    matrix_(3,0) = 0;
    matrix_(3,1) = 0;
    matrix_(3,2) = 0;
    matrix_(3,3) = 1;
}

Transform::Transform(const boost::array<float, 16> matrix)
{
    for (int r = 0; r < 4; r++)
    {
        for (int c = 0; c < 4; c++)
        {
            matrix_(r,c) = matrix[4*r+c];
        }
    }
}

Transform::Transform(const cv::Mat& matrix)
{
	assert(matrix.rows == 4);
	assert(matrix.cols == 4);

	for (int r = 0; r < 4; r++)
	{
		 for (int c = 0; c < 4; c++)
		 {
			  matrix_(r,c) = matrix.at<double>(r,c);
		 }
	}
}

Transform::Transform(const cv::Matx44d& matrix)
{
	for (int r = 0; r < 4; r++)
	{
		 for (int c = 0; c < 4; c++)
		 {
			  matrix_(r,c) = matrix(r,c);
		 }
	}
}

Transform::Transform(const boost::array<float, 12> matrix)
{
    for (int r = 0; r < 3; r++)
    {
        for (int c = 0; c < 4; c++)
        {
            matrix_(r,c) = matrix[4*r+c];
        }
    }
    matrix_(3,0) = 0;
    matrix_(3,1) = 0;
    matrix_(3,2) = 0;
    matrix_(3,3) = 1;
}

Transform::Transform(double Ra, double Rb, double Rc, double x, double y, double z)
{
    init(Ra, Rb, Rc, x, y, z, MathOp::ZYX_EULER);
}

Transform::Transform(double Ra, double Rb, double Rc, double x, double y, double z, MathOp::rotation rot_type)
{
    init(Ra, Rb, Rc, x, y, z, rot_type);
}

Transform::Transform(const cv::Vec3d rot, const cv::Vec3d trans)
{
    init(	(double)rot[0],
            (double)rot[1],
            (double)rot[2],
            (double)trans[0],
            (double)trans[1],
            (double)trans[2],
            MathOp::ZYX_EULER);
}

Transform::Transform(const cv::Vec3d rot, const cv::Vec3d trans, MathOp::rotation rot_type)
{
    init(	(double)rot[0],
            (double)rot[1],
            (double)rot[2],
            (double)trans[0],
            (double)trans[1],
            (double)trans[2],
            rot_type);
}

Transform::Transform(const Eigen::Vector3d rot, const Eigen::Vector3d trans)
{
    init(	(double)rot[0],
            (double)rot[1],
            (double)rot[2],
            (double)trans[0],
            (double)trans[1],
            (double)trans[2],
            MathOp::ZYX_EULER);
}

Transform::Transform(const cv::Vec6d vec)
{
    init(	(double)vec[3],
            (double)vec[4],
            (double)vec[5],
            (double)vec[0],
            (double)vec[1],
            (double)vec[2],
            MathOp::ZYX_EULER);
}

Transform::Transform(const Eigen::Matrix3d& matrix, const cv::Vec3d trans)
{
    init(matrix, trans[0], trans[1], trans[2]);
}

Transform::Transform(const geometry_msgs::Transform transform)
{
    geometry_msgs::Quaternion r = transform.rotation;
    geometry_msgs::Vector3 t = transform.translation;
    Eigen::Quaternion<double> rot_quaternions(r.w, r.x, r.y, r.z);
	 Eigen::Matrix3d matrix_temp = rot_quaternions.toRotationMatrix();
    init(matrix_temp, t.x, t.y, t.z);
}

Transform::Transform(const geometry_msgs::Pose pose)
{
	geometry_msgs::Quaternion r = pose.orientation;
	Eigen::Quaternion<double> rot_quaternions(r.w, r.x, r.y, r.z);
	Eigen::Matrix3d matrix_temp = rot_quaternions.toRotationMatrix();
	init(matrix_temp, pose.position.x, pose.position.y, pose.position.z);
}

Transform::Transform(double x, double y, double z, double rw, double rx, double ry, double rz)
{
    Eigen::Quaternion<double> rot_quaternions(rw, rx, ry, rz);
    Eigen::Matrix3d matrix_temp = rot_quaternions.toRotationMatrix();
    init(matrix_temp, x, y, z);
}

Transform::Transform(std::vector<double> quaternion_frame)
{
    Eigen::Quaternion<double> rot_quaternions(quaternion_frame[3], quaternion_frame[4], quaternion_frame[5], quaternion_frame[6]);
    Eigen::Matrix3d matrix_temp = rot_quaternions.toRotationMatrix();
    init(matrix_temp, quaternion_frame[0], quaternion_frame[1], quaternion_frame[2]);
}

// From: http://inside.mines.edu/~gmurray/ArbitraryAxisRotation/
Transform Transform::setRotationAboutOriginVector(cv::Vec3d vec, double angle)
{
    return setRotationAboutOriginVector(vec, angle, cv::Point3d(0,0,0));
}

// See angvec2r in rvctools
Transform Transform::setRotationAboutOriginVector(cv::Vec3d vec, double angle, cv::Point3d trans)
{
    using namespace std;

    double cth = cos(angle);
    double sth = sin(angle);
    double vth = (1 - cth);

    vec = cv::normalize(vec);
    double kx = vec[0];
    double ky = vec[1];
    double kz = vec[2];

    matrix_(0,0) = kx*kx*vth+cth;
    matrix_(0,1) = ky*kx*vth-kz*sth;
    matrix_(0,2) = kz*kx*vth+ky*sth;
    matrix_(0,3) = trans.x;
    matrix_(1,0) = kx*ky*vth+kz*sth;
    matrix_(1,1) = ky*ky*vth+cth;
    matrix_(1,2) = kz*ky*vth-kx*sth;
    matrix_(1,3) = trans.y;
    matrix_(2,0) = kx*kz*vth-ky*sth;
    matrix_(2,1) = ky*kz*vth+kx*sth;
    matrix_(2,2) = kz*kz*vth+cth;
    matrix_(2,3) = trans.z;
    matrix_(3,0) = 0;
    matrix_(3,1) = 0;
    matrix_(3,2) = 0;
    matrix_(3,3) = 1;

    return *this;
}

Transform Transform::rotateAroundZ(double angle)
{
    using namespace std;

    Eigen::Matrix4d R;
    R(0,0) = cos(angle);
    R(0,1) = -sin(angle);
    R(0,2) = 0;
    R(0,3) = 0;
    R(1,0) = sin(angle);
    R(1,1) = cos(angle);
    R(1,2) = 0;
    R(1,3) = 0;
    R(2,0) = 0;
    R(2,1) = 0;
    R(2,2) = 1;
    R(2,3) = 0;
    R(3,0) = 0;
    R(3,1) = 0;
    R(3,2) = 0;
    R(3,3) = 1;

    matrix_ = matrix_ * R;
    return *this;
}

Transform& Transform::setTranslation(double x, double y, double z)
{
	matrix_(0,3) = x;
	matrix_(1,3) = y;
	matrix_(2,3) = z;
	return *this;
}

Transform Transform::inverse() const
{
    return Transform(matrix_.inverse());
}

Transform Transform::convertToMeters() const
{
    Eigen::Matrix4d matrix = matrix_;
    matrix(0,3) /= 1000.0;
    matrix(1,3) /= 1000.0;
    matrix(2,3) /= 1000.0;

    return Transform(matrix);
}

Transform Transform::convertToMm() const
{
    Eigen::Matrix4d matrix = matrix_;
    matrix(0,3) *= 1000.0;
    matrix(1,3) *= 1000.0;
    matrix(2,3) *= 1000.0;

    return Transform(matrix);
}

cv::Vec3d Transform::getEuler() const
{
    //FIXME: Obsolete! Use default parameter on the overloaded function below! (din pong-abe!)
    Eigen::Matrix3d rotation_matrix(3,3);
    for (int row = 0; row < 3; row++)
    {
        for (int col = 0; col < 3; col++)
        {
            rotation_matrix(row, col) = matrix_(row, col);
        }
    }
    Eigen::Vector3d euler = rotation_matrix.eulerAngles(/*yaw*/2, /*pitch*/1, /*roll*/0);
    return cv::Vec3d(euler[0], euler[1], euler[2]);
}

cv::Vec3d Transform::getEuler(MathOp::rotation rot) const
{
    Eigen::Matrix3d rotation_matrix(3,3);
    for (int row = 0; row < 3; row++)
    {
        for (int col = 0; col < 3; col++)
        {
            rotation_matrix(row, col) = matrix_(row, col);
        }
    }

    Eigen::Vector3d euler;
    switch (rot) {
    case MathOp::ZYX_EULER:
        euler = rotation_matrix.eulerAngles(/*yaw*/2, /*pitch*/1, /*roll*/0);
        break;
    case MathOp::ZXZ_EULER:
        euler = rotation_matrix.eulerAngles(/*yaw*/2, /*pitch*/0, /*yaw*/2);
        break;
    default:
        euler = rotation_matrix.eulerAngles(/*yaw*/2, /*pitch*/1, /*roll*/0);
        cerr << "None supported type for this function. Returning ZYX Euler angles! This WILL cause undefined and catastrophic behaviour!" << endl;
        break;
    }

    return cv::Vec3d(euler[0], euler[1], euler[2]);
}

cv::Vec3d Transform::getTranslation() const
{
    cv::Vec3d t = cv::Vec3d(matrix_(0,3), matrix_(1,3), matrix_(2,3));
    return t;
}

cv::Vec4d Transform::getQuaternions() const
{
    Eigen::Matrix3d rotation_matrix(3,3);
    for (int row = 0; row < 3; row++)
    {
        for (int col = 0; col < 3; col++)
        {
            rotation_matrix(row, col) = matrix_(row, col);
        }
    }
    Eigen::Quaternion<double> rot_quaternions(rotation_matrix);

    return cv::Vec4d(rot_quaternions.w(), rot_quaternions.x(), rot_quaternions.y(), rot_quaternions.z());
}

std::vector<double> Transform::getFullQuanternionTFVector()
{
    cv::Vec4d rot_quaternions = getQuaternions();
    cv::Vec3d translation(matrix_(0,3), matrix_(1,3), matrix_(2,3));

    std::vector<double> full_quaternion_tf_vector;
    full_quaternion_tf_vector.push_back( (double)translation[0] );
    full_quaternion_tf_vector.push_back( (double)translation[1] );
    full_quaternion_tf_vector.push_back( (double)translation[2] );
    full_quaternion_tf_vector.push_back( (double)rot_quaternions[0] );
    full_quaternion_tf_vector.push_back( (double)rot_quaternions[1] );
    full_quaternion_tf_vector.push_back( (double)rot_quaternions[2] );
    full_quaternion_tf_vector.push_back( (double)rot_quaternions[3] );

    return full_quaternion_tf_vector;
}

cv::Vec6d Transform::getVector(bool degrees) const
{
	 cv::Vec3d angles = getEuler();
	 if (degrees)
	 {
		 for (int i = 0; i < 3; i++)
			 angles = angles * 180.0/M_PI;
	 }
    cv::Vec6d vec(matrix_(0,3), matrix_(1,3), matrix_(2,3),
                  angles[0], angles[1], angles[2]);


    return vec;
}

cv::Vec6d Transform::getVector(MathOp::rotation rot) const
{

    Eigen::Matrix3d rotation_matrix(3,3);
    for (int row = 0; row < 3; row++)
    {
        for (int col = 0; col < 3; col++)
        {
            rotation_matrix(row, col) = matrix_(row, col);
        }
    }

    cv::Vec3d angles;

    switch (rot) {
    case MathOp::AXISANGLE:
    {
        Eigen::AngleAxisd angvec(rotation_matrix);
        Eigen::Vector3d axis = angvec.axis();
        double theta = angvec.angle();

        if(theta > M_PI)
        {
            //            std::cout << endl << " Theta fisk 2 " << endl;
            theta = (theta - M_PI) - M_PI;
        }
        for(int i = 0; i < 3; i++)
        {
            angles[i] = axis[i]*theta;
        }
    }
        break;
    case MathOp::ZYX_EULER:
        angles = getEuler(MathOp::ZYX_EULER);
        break;
    case MathOp::ZXZ_EULER:
        angles = getEuler(MathOp::ZXZ_EULER);
        break;
    default:
        break;
    }

    cv::Vec6d vec(matrix_(0,3), matrix_(1,3), matrix_(2,3),
                  angles[0], angles[1], angles[2]);
    return vec;
}

vector<double> Transform::getStdVector(bool degrees) const
{
    cv::Vec3d r = getEuler(MathOp::ZYX_EULER);

    cv::Vec3d t = cv::Vec3d(matrix_(0,3), matrix_(1,3), matrix_(2,3));

    if(degrees)
    {
        r = (180/M_PI) * r;
    }

    vector<double> frame;

    frame.push_back(t[0]);
    frame.push_back(t[1]);
    frame.push_back(t[2]);
    frame.push_back(r[0]);
    frame.push_back(r[1]);
    frame.push_back(r[2]);

    return frame;
}

geometry_msgs::Transform Transform::getGeometryMsgsTransform() const
{
    cv::Vec3d translation = getTranslation();
    cv::Vec4d rotation = getQuaternions();

    geometry_msgs::Transform transform;
    transform.translation.x = translation[0];
    transform.translation.y = translation[1];
    transform.translation.z = translation[2];
    transform.rotation.w = rotation[0];
    transform.rotation.x = rotation[1];
    transform.rotation.y = rotation[2];
    transform.rotation.z = rotation[3];

    return transform;
}

geometry_msgs::TransformStamped Transform::getGeometryMsgsTransformStamped() const
{
	geometry_msgs::TransformStamped transform_stamped;
	transform_stamped.header.stamp = ros::Time::now();
	transform_stamped.transform = getGeometryMsgsTransform();

	return transform_stamped;
}

geometry_msgs::Pose Transform::getGeometryMsgsPose() const
{
	 cv::Vec3d translation = getTranslation();
	 cv::Vec4d rotation = getQuaternions();

	 geometry_msgs::Pose pose;
	 pose.position.x = translation[0];
	 pose.position.y = translation[1];
	 pose.position.z = translation[2];
	 pose.orientation.w = rotation[0];
	 pose.orientation.x = rotation[1];
	 pose.orientation.y = rotation[2];
	 pose.orientation.z = rotation[3];

	 return pose;
}

tf2::Transform Transform::getTf2Transform() const
{
    tf2::Matrix3x3 rot;
    tf2::Vector3 trans;
    for (int r = 0; r < 3; r++)
    {
        for (int c = 0; c < 3; c++)
        {
            rot[r][c] = matrix_(r,c);
        }
        trans[r] = matrix_(r,3);
    }

    tf2::Transform tf;
    tf.setBasis(rot);
    tf.setOrigin(trans);

    return tf;
}

void Transform::getGlArray(double ret_val[16]) const
{
	// OpelGL uses column-major matrix layout:
	//   [ 0   4   8  12 ]
	//   [ 1   5   9  13 ]
	//   [ 2   6  10  14 ]
	//   [ 3   7  11  15 ]

	ret_val[0]  = matrix_(0,0);
	ret_val[4]  = matrix_(0,1);
	ret_val[8]  = matrix_(0,2);
	ret_val[12] = matrix_(0,3);
	ret_val[1]  = matrix_(1,0);
	ret_val[5]  = matrix_(1,1);
	ret_val[9]  = matrix_(1,2);
	ret_val[13] = matrix_(1,3);
	ret_val[2]  = matrix_(2,0);
	ret_val[6]  = matrix_(2,1);
	ret_val[10] = matrix_(2,2);
	ret_val[14] = matrix_(2,3);
	ret_val[3]  = matrix_(3,0);
	ret_val[7]  = matrix_(3,1);
	ret_val[11] = matrix_(3,2);
	ret_val[15] = matrix_(3,3);
}

void Transform::init(const Eigen::Matrix3d& matrix, double x, double y, double z)
{
    matrix_(0,0) = matrix(0,0);
    matrix_(0,1) = matrix(0,1);
    matrix_(0,2) = matrix(0,2);
    matrix_(0,3) = x;
    matrix_(1,0) = matrix(1,0);
    matrix_(1,1) = matrix(1,1);
    matrix_(1,2) = matrix(1,2);
    matrix_(1,3) = y;
    matrix_(2,0) = matrix(2,0);
	 matrix_(2,1) = matrix(2,1);
	 matrix_(2,2) = matrix(2,2);
    matrix_(2,3) = z;
    matrix_(3,0) = 0;
    matrix_(3,1) = 0;
    matrix_(3,2) = 0;
    matrix_(3,3) = 1;
}

void Transform::init(double Ra, double Rb, double Rc, double x, double y, double z, MathOp::rotation rot)
{

    switch (rot)
    {
    case MathOp::ZYX_EULER:
        matrix_(0,0) = std::cos(Ra) * std::cos(Rb);
        matrix_(0,1) = std::cos(Ra) * std::sin(Rb) * std::sin(Rc) - std::sin(Ra) * std::cos(Rc);
        matrix_(0,2) = std::cos(Ra) * std::sin(Rb) * std::cos(Rc) + std::sin(Ra) * std::sin(Rc);
        matrix_(0,3) = x;
        matrix_(1,0) = std::sin(Ra) * std::cos(Rb);
        matrix_(1,1) = std::sin(Ra) * std::sin(Rb) * std::sin(Rc) + std::cos(Ra) * std::cos(Rc);
        matrix_(1,2) = std::sin(Ra) * std::sin(Rb) * std::cos(Rc) - std::cos(Ra) * std::sin(Rc);
        matrix_(1,3) = y;
        matrix_(2,0) = -std::sin(Rb);
        matrix_(2,1) = std::cos(Rb) * std::sin(Rc);
        matrix_(2,2) = std::cos(Rb) * std::cos(Rc);
        matrix_(2,3) = z;
        matrix_(3,0) = 0;
        matrix_(3,1) = 0;
        matrix_(3,2) = 0;
        matrix_(3,3) = 1;
        break;
    case MathOp::AXISANGLE:
    {
        cv::Vec3d axis_temp(Ra,Rb,Rc);
        double theta = cv::norm(axis_temp);
        if(theta > M_PI)
        {
            //            std::cout << endl << " Theta fisk 1 " << endl;
            theta = (theta - M_PI) - M_PI;
        }

        axis_temp = cv::normalize(axis_temp);
        Eigen::Vector3d axis(axis_temp[0],axis_temp[1],axis_temp[2]);
        Eigen::AngleAxisd angvec(theta,axis);
        Eigen::Matrix3d matrix_temp = angvec.toRotationMatrix();
        init(matrix_temp, x, y, z);

        //cv::Vec3d trans(x,y,z);
        //setRotationAboutAbitraryVector(angles,theta,trans);
    }
        break;
    default:
        break;
    }
}

string Transform::print() const
{
    stringstream ss;

    ss << "[" << endl;
    for (int row = 0; row < 4; row++)
    {
        ss << "[";
        for (int col = 0; col < 4; col++)
        {
            ss << matrix_(row,col) << ", ";
        }
        if (row != 3)
            ss << "], " << endl;
    }
    ss << "] ]";

    return ss.str();
}

string Transform::printVector() const
{
    stringstream ss;

    cv::Vec6d vec = getVector();

    ss << "[";
    for (int i = 0; i < 6; i++)
    {
        ss << vec(i);
        if (i < 5)
            ss << ", ";
    }
    ss << "]";

    return ss.str();
}

string Transform::printInfo() const
{
    cv::Vec3d z_rot = (*this) * cv::Vec3d(0,0,1);
    stringstream ss;
    ss << printVector();
    ss << ", z = [" << z_rot[0] << ", " << z_rot[1] << ", " << z_rot[2] << "]";
    return ss.str();
}

int Transform::saveTo(string path, int rows, int cols) const
{
    if (rows <= 0 || rows > 4 || cols <= 0 || cols > 4)
    {
        cerr << "Transform::saveTo called with invalid parameters: rows="
             << rows << ", cols=" << cols << endl;
    }

    ofstream myfile(path.c_str());
    if (!myfile.is_open())
    {
        cerr << "Unable to create file: " << path;
        return -1;
    }

    for (int r = 0; r < rows; r++)
    {
        for (int c = 0; c < cols; c++)
        {
            myfile << matrix_(r,c) << " ";
        }
        myfile << "\n";
    }
    myfile.close();

    return 0;
}

int Transform::loadFrom(string path)
{
    ifstream myfile(path.c_str());
    if (!myfile.is_open())
    {
        cerr << "Unable to open file: " << path;
        return -1;
    }

    string line;
    //int r = 0;
    for (int r = 0; r < 4; r++)
    {
        if (!getline(myfile,line))
        {
            cerr << "Could not read row " << r << " of matrix" << endl;
            return -1;
        }
        for (int c = 0; c < 4; c++)
        {
            int pos = line.find(" ");
            string word = line.substr(0, pos);
            line.erase(0, pos + 1);

            // Parse word and add to values:
            double temp = ::strtod(word.c_str(), 0);
            matrix_(r,c) = temp;
        }
    }
    myfile.close();

    return 0;
}

const Transform Transform::operator*(const Transform& rhs) const
{
    Eigen::Matrix4d m = matrix_ * rhs.matrix_;
    return Transform(m);
}

const cv::Vec3d Transform::operator*(const cv::Vec3d& rhs) const
{
    cv::Vec3d retVal;
    retVal[0] = matrix_(0,0) * rhs[0] + matrix_(0,1) * rhs[1] + matrix_(0,2) * rhs[2];
    retVal[1] = matrix_(1,0) * rhs[0] + matrix_(1,1) * rhs[1] + matrix_(1,2) * rhs[2];
    retVal[2] = matrix_(2,0) * rhs[0] + matrix_(2,1) * rhs[1] + matrix_(2,2) * rhs[2];

    return retVal;
}

const cv::Point3d Transform::operator*(const cv::Point3d& rhs) const
{
    cv::Point3d retVal;
    retVal.x = matrix_(0,0) * rhs.x + matrix_(0,1) * rhs.y + matrix_(0,2) * rhs.z + matrix_(0,3);
    retVal.y = matrix_(1,0) * rhs.x + matrix_(1,1) * rhs.y + matrix_(1,2) * rhs.z + matrix_(1,3);
    retVal.z = matrix_(2,0) * rhs.x + matrix_(2,1) * rhs.y + matrix_(2,2) * rhs.z + matrix_(2,3);

    return retVal;
}

const cv::Vec4d Transform::operator*(const cv::Vec4d& rhs) const
{
	cv::Vec4d retVal;
	retVal[0] = matrix_(0,0)*rhs[0] + matrix_(0,1)*rhs[1] + matrix_(0,2)*rhs[2] + matrix_(0,3)*rhs[3];
	retVal[1] = matrix_(1,0)*rhs[0] + matrix_(1,1)*rhs[1] + matrix_(1,2)*rhs[2] + matrix_(1,3)*rhs[3];
	retVal[2] = matrix_(2,0)*rhs[0] + matrix_(2,1)*rhs[1] + matrix_(2,2)*rhs[2] + matrix_(2,3)*rhs[3];
	retVal[3] = matrix_(3,0)*rhs[0] + matrix_(3,1)*rhs[1] + matrix_(3,2)*rhs[2] + matrix_(3,3)*rhs[3];

	return retVal;
}

const Transform Transform::operator+(const cv::Vec3d& rhs) const
{
    Eigen::Matrix4d matrix = matrix_;
    matrix(0,3) += rhs[0];
    matrix(1,3) += rhs[1];
    matrix(2,3) += rhs[2];

    return Transform(matrix);
}

const Transform Transform::operator-(const cv::Vec3d& rhs) const
{
    Eigen::Matrix4d matrix = matrix_;
    matrix(0,3) -= rhs[0];
    matrix(1,3) -= rhs[1];
    matrix(2,3) -= rhs[2];

    return Transform(matrix);
}

double findAngle(const cv::Vec3d& v1, const cv::Vec3d& v2)
{
    double cos_angle = v1.ddot(v2) / (sqrt(v1[0]*v1[0]+v1[1]*v1[1]+v1[2]*v1[2]) *
            sqrt(v2[0]*v2[0]+v2[1]*v2[1]+v2[2]*v2[2]));
    return acos(cos_angle);
}


/************* Vector ***************/
//Vector Vector::dot(const Vector& v) const
//{
//	return Vector(vec_[0]*v[0], vec_[1]*v[1], vec_[2]*v[2]);
//}

//Vector Vector::cross(const Vector& rhs) const
//{
//	Vector ret_val(vec_[1]*rhs[2] - vec_[2]*rhs[1],
//				   vec_[2]*rhs[0] - vec_[0]*rhs[2],
//				   vec_[0]*rhs[1] - vec_[1]*rhs[0]);
//	return ret_val;
//}

//double Vector::operator[](int element) const
//{
//	return vec_[element];
//}

cv::Vec3d Vector::CrossProduct(cv::Vec3d vec1, cv::Vec3d vec2)
{
    return vec1.cross(vec2);
}

double Vector::DotProduct(cv::Vec3d vec1, cv::Vec3d vec2)
{
    return vec1.dot(vec2);
}


}



